DROP database easyfoot;

CREATE DATABASE easyfoot;

USE easyfoot;

select * from administrator;
insert into administrator values(1, 'admin', 'senha');

SELECT * FROM player;
SELECT * FROM team;
SELECT * FROM team_players;
SELECT * FROM game;
SELECT * FROM place;
SELECT * FROM team_games;
SELECT * from conversation;
SELECT * from team_ranking;
select * from registration;
select * from city;

DELETE FROM team_ranking WHERE TEAM_ID = 5 and id = 27

INSERT INTO player VALUES (1, 'tiago@email.com.br', 	null, 'Tiago', 	'senha', null);
INSERT INTO player VALUES (2, 'matheus@email.com.br',	null, 'Matheus', 'senha', null);
INSERT INTO player VALUES (3, 'alex@email.com.br',		null, 'Alex', 	'senha', null);
INSERT INTO player VALUES (4, 'neves@email.com.br',		null, 'Neves', 	'senha', null);
INSERT INTO player VALUES (5, 'mario@email.com.br',		null, 'Mario', 'senha', null);
INSERT INTO player VALUES (6, 'razer@email.com.br',		null, 'Razer', 'senha', null);
INSERT INTO player VALUES (7, 'rafaela@email.com.br',	null, 'Rafaela', 	'senha', null);

INSERT INTO country VALUES(1, 'Brasil');
INSERT INTO state VALUES(1, 'Paraná', 1);
INSERT INTO city VALUES(1, 'Curitiba', 1);

INSERT INTO team VALUES(1, 'TADS Perna de Pau', 1, null, 1);
INSERT INTO team VALUES(2, 'Bianca FutClub', 1, null, 1);

INSERT INTO team_players VALUES(1,1);
INSERT INTO team_players VALUES(2,1);

INSERT INTO place VALUES(1, 2505.45, 5463.33, 'Praça Osvaldo Cruz', 1);
INSERT INTO place VALUES(2, 5487.55, 2546.22,'Arthur Bernardes', 1);
INSERT INTO place VALUES(3, 5487.87, 2546.79, 'Praça Fernando Alves', 1);

INSERT INTO conversation VALUES(1, 'Oi', 1, 2);
INSERT INTO conversation VALUES(2, 'Oi, tudo bem?', 2, 1);
INSERT INTO conversation VALUES(3, 'Aqui tranquilo, bora apresentar aquele TCC?', 1, 2);

INSERT INTO team_games VALUES(1,1);
INSERT INTO team_games VALUES(1,2);
UPDATE GAME SET confirmed = true where id = 2