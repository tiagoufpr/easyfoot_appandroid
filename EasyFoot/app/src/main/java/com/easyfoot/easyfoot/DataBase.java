package com.easyfoot.easyfoot;

/**
 * Created by Tiago Schernoveber on 15/08/2018.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {
    String SQLCreate1 =     "CREATE TABLE IF NOT EXISTS CONFIGURATIONS(                                   " +
                            "   loginSave               CHAR,                                             " +
                            "   loginEmail              VARCHAR(200),                                     " +
                            "   loginPassword           VARCHAR(200),                                     " +
                            "   notificationsSave               CHAR                                      " +
                            ")                                                                            ";


    public DataBase(Context context){
        super(context, "EasyFoot", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQLCreate1);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
