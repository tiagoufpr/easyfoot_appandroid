package com.easyfoot.easyfoot;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class ConfigurationsActivity extends AppCompatActivity {

    private DataBase dataBase;
    private SQLiteDatabase conn;
    private Cursor cursor;

    String sqlSelectConfigurations = "SELECT * FROM CONFIGURATIONS";

    private CheckBox cbNotifications, cbLogin;
    private Button btnSalvar;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configurations);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cbNotifications = (CheckBox) findViewById(R.id.cbNotifications);
        cbLogin = (CheckBox) findViewById(R.id.cbLogin);
        btnSalvar = (Button) findViewById(R.id.btnSalvar);

        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveConfigurations();

                Intent intent = new Intent(ConfigurationsActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        loadConfigurations();
    }

    public void loadConfigurations() {
        try {
            dataBase = new DataBase(this);
            conn = dataBase.getWritableDatabase();

            cursor = conn.rawQuery(sqlSelectConfigurations, null);


            if (cursor.getCount() > 0) {

                cursor.moveToFirst();

                cbLogin.setChecked(cursor.getString(cursor.getColumnIndex("loginSave")).trim().equals("S"));
                cbNotifications.setChecked(cursor.getString(cursor.getColumnIndex("notificationsSave")).trim().equals("S"));

            }

            conn.close();
        } catch (SQLException ex) {
            Toast.makeText(getApplicationContext(), com.easyfoot.easyfoot.R.string.configurations_error_load, Toast.LENGTH_SHORT).show();
        }
    }

    public void saveConfigurations() {
        try {
            dataBase = new DataBase(ConfigurationsActivity.this);
            conn = dataBase.getWritableDatabase();

            ContentValues row = new ContentValues();


            if (cbLogin.isChecked()) {
                row.put("loginSave", "S");
                row.put("loginEmail", MainActivity.loggedPlayer.getEmail());
                row.put("loginPassword", MainActivity.loggedPlayer.getPassword());
            } else {
                row.put("loginSave", "N");
                row.put("loginEmail", "");
                row.put("loginPassword", "");
            }

            if (cbNotifications.isChecked())
                row.put("notificationsSave", "S");
            else
                row.put("notificationsSave", "N");

            
            cursor = conn.rawQuery(sqlSelectConfigurations, null);


            if (cursor.getCount() > 0)
                conn.update("CONFIGURATIONS", row, null, null);
            else
                conn.insert("CONFIGURATIONS", null, row);



            conn.close();

            Toast.makeText(getApplicationContext(), com.easyfoot.easyfoot.R.string.configurations_saved, Toast.LENGTH_SHORT).show();

        } catch (SQLException ex) {
            Toast.makeText(getApplicationContext(), com.easyfoot.easyfoot.R.string.configurations_error_save, Toast.LENGTH_SHORT).show();
        }
    }
}
