package com.easyfoot.easyfoot.util;

import android.graphics.Bitmap;
import android.util.Base64;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import kotlin.collections.MapAccessorsKt;

/**
 * Created by Tiago.
 */

public class ImageUtils {

    public String BitmapToString(Bitmap image){
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);

        byte[] byteArray = byteArrayOutputStream .toByteArray();

        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public String uploadImage(String imageStr){
            Cloudinary cloudinary =new Cloudinary("cloudinary://795526482314357:FGQgw2SlZUyDH-Ds4pjJmXIv7u4@dfyy9ttom");
            try {
                Map response = cloudinary.uploader().upload("data:image/png;base64,"+imageStr, ObjectUtils.emptyMap());
                String urlImg = (String)response.get("url");

                return urlImg;

            }catch (Exception e){
                e.printStackTrace();
            }
            return "";
    }


}
