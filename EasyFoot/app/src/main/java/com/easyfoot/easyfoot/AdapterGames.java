package com.easyfoot.easyfoot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.easyfoot.easyfoot.model.Game;
import com.easyfoot.easyfoot.model.Team;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdapterGames extends BaseAdapter {
    private final Activity activity;
    private final List<Game> games;
    SimpleDateFormat simpleDate;

    public AdapterGames(Activity activity, List<Game> games) {
        this.activity = activity;
        this.games = games;
        this.simpleDate =  new SimpleDateFormat("dd/MM/yyyy");
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.adapter_games, parent, false);

        final Game game = games.get(position);

        if(game != null) {
            TextView name = (TextView) view.findViewById(R.id.adapter_game_name);
            TextView date = (TextView) view.findViewById(R.id.adapter_game_date);
            TextView place = (TextView) view.findViewById(R.id.adapter_game_place);

            name.setText(game.getName().toString());
            date.setText(simpleDate.format(game.getDateTime()));
            place.setText(game.getPlace().getName().toString());
            //imagem.setImageResource(R.drawable.java);

            LinearLayout llGame = (LinearLayout) view.findViewById(R.id.llGame);

            llGame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if((game.getCreatedBy().getId().equals(MainActivity.loggedPlayer.getId())) && game.getConfirmed() ){
                        if (activity.getClass() != MatchesActivity.class){
                            Intent intent = new Intent(activity, MatchesActivity.class);
                            activity.startActivity(intent);
                        } else if (activity.getClass() == MatchesActivity.class) {
                            if (game.getTeams().get(0).getCaptain().getId().equals(MainActivity.loggedPlayer.getId())) {
                                MatchesActivity.startSetResult(game.getTeams().get(0), game.getTeams().get(1));
                            } else {
                                MatchesActivity.startSetResult(game.getTeams().get(1), game.getTeams().get(0));
                            }

                            MatchesActivity.game = game;
                        }
                    } else if (!game.getConfirmed()){
                        Toast.makeText(activity, activity.getString(R.string.matches_error4), Toast.LENGTH_SHORT).show();


                    } else {
                        Toast.makeText(activity, activity.getString(R.string.matches_error3) +" "+ game.getCreatedBy().getName(), Toast.LENGTH_SHORT).show();
                    }
                }
            });

            Button btnRemoveGame = (Button) view.findViewById(R.id.btnRemoveGame);
            Button btnAcceptGame = (Button) view.findViewById(R.id.btnAcceptGame);
            Button btnRefuseGame = (Button) view.findViewById(R.id.btnRefuseGame);

            if (activity.getClass() == MainActivity.class) {
                btnRemoveGame.setVisibility(View.GONE);
                btnAcceptGame.setVisibility(View.GONE);
                btnRefuseGame.setVisibility(View.GONE);
            } else {
                //Set<Team> teams = game.getTeams();
                List<Team> listTeams = new ArrayList<>(game.getTeams());

                //if(listTeams.size() > 0) {
                    //if (listTeams.get(0).getCaptain() == MainActivity.loggedPlayer) {
                        if (game.getConfirmed()) {
                            btnRemoveGame.setVisibility(View.VISIBLE);
                            btnAcceptGame.setVisibility(View.GONE);
                            btnRefuseGame.setVisibility(View.GONE);

                            btnRemoveGame.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    MatchesActivity.refuseMatch(game);
                                }
                            });
                        } else {
                            btnRemoveGame.setVisibility(View.GONE);

                            if (game.getCreatedBy().getId().equals(MainActivity.loggedPlayer.getId()))
                                btnAcceptGame.setVisibility(View.GONE);
                            else
                                btnAcceptGame.setVisibility(View.VISIBLE);

                            btnRefuseGame.setVisibility(View.VISIBLE);

                            btnAcceptGame.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    MatchesActivity.acceptMatch(game);
                                }
                            });
                            btnRefuseGame.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    MatchesActivity.refuseMatch(game);
                                }
                            });
                        }
                    }
                //}
            //}
        }
        return view;
    }
}

