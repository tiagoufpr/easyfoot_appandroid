package com.easyfoot.easyfoot;

import android.app.Activity;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Conversation;
import com.easyfoot.easyfoot.model.Player;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {
    static Activity activity;
    private Intent intent;

    private static ConstraintLayout clMainChat;
    //private static ConstraintLayout clPlayerSelector;
    private static LinearLayout clConversation;

    private Button btnNewConversation;

    private List<Player> players;
    private static ListView lvConversations;
    private static AdapterConversations adapterConversations;

    private Spinner spnPlayerType;
    private EditText edtPlayerValue;
    private Button btnPlayerSearch;
    //private static ListView lvPlayerSelector;
    //private static List<Player> playersSelector;
    //private static AdapterPlayers adapterPlayersSelector;
    private Button btnPlayerSearchCancel;

    private static Thread tConversation;
    private static List<Conversation> conversations;
    private static ListView lvConversation;
    private static AdapterConversation adapterConversation;
    private static Player playerTalkingTo;
    private EditText edtMessage;
    private Button btnSendMessage;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if(tConversation.isAlive())
            tConversation.interrupt();

        switch (item.getItemId()){
            case android.R.id.home:
                if(clMainChat.getVisibility() == View.GONE){
                    clConversation.setVisibility(View.GONE);
                    //clPlayerSelector.setVisibility(View.GONE);
                    getPlayers();
                    clMainChat.setVisibility(View.VISIBLE);
                    setTitle("Chat");
                } else
                    this.finish();

                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        intent = new Intent();

        activity = ChatActivity.this;

        clMainChat = (ConstraintLayout) findViewById(R.id.clMainChat);
        //clPlayerSelector = (ConstraintLayout) findViewById(R.id.clPlayerSelector);
        clConversation = (LinearLayout) findViewById(R.id.clConversation);

        btnNewConversation = (Button) findViewById(R.id.btnNewConversation);

        spnPlayerType = (Spinner) findViewById(R.id.spnPlayerType);
        edtPlayerValue = (EditText) findViewById(R.id.edtPlayerValue);
        btnPlayerSearch = (Button) findViewById(R.id.btnPlayerSearch);
        //lvPlayerSelector = (ListView) findViewById(R.id.lvPlayerSelector);
        btnPlayerSearchCancel = (Button) findViewById(R.id.btnPlayerSearchCancel);

        lvConversations = (ListView) findViewById(R.id.lvConversations);
        getPlayers();

        lvConversation = (ListView) findViewById(R.id.lvConversation);
        edtMessage = (EditText) findViewById(R.id.edtMessage);
        btnSendMessage = (Button) findViewById(R.id.btnSendMessage);

        /*
        btnNewConversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playerTalkingTo = null;
                edtPlayerValue.setText("");
                clMainChat.setVisibility(View.GONE);
                clConversation.setVisibility(View.GONE);
                clPlayerSelector.setVisibility(View.VISIBLE);
            }
        });


        btnPlayerSearchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playerTalkingTo = null;
                edtPlayerValue.setText("");
                clPlayerSelector.setVisibility(View.GONE);
                clConversation.setVisibility(View.GONE);
                clMainChat.setVisibility(View.VISIBLE);
            }
        });
        */

        btnNewConversation.setOnClickListener(new View.OnClickListener() {
            @Override
                 public void onClick(View view) {
                    playerTalkingTo = null;

                    intent.putExtra("sender", "ChatActivity");

                    intent.setClass(ChatActivity.this, PlayerSearchActivity.class);
                    startActivity(intent);
                 }
        });

        /*
        btnPlayerSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtPlayerValue.length() == 0) {
                    edtPlayerValue.setError(getString(R.string.error_field));

                    if (edtPlayerValue.isFocusable())
                        edtPlayerValue.requestFocus();
                } else {


                    playersSelector = new ArrayList <Player>();

                    if (spnPlayerType.getSelectedItemId() == 0) {
                        Call <Player> requestPlayer = new RetrofitConfig().getEndpointsService().getPlayerById(Long.parseLong(edtPlayerValue.getText().toString()));
                        requestPlayer.enqueue(new Callback <Player>() {
                            @Override
                            public void onResponse(Call <Player> call, Response <Player> response) {
                                if (response.isSuccessful()) {
                                    Player player = response.body();

                                    playersSelector.add(player);

                                    adapterPlayersSelector = new AdapterPlayers(ChatActivity.this, playersSelector);
                                    lvPlayerSelector.setAdapter(adapterPlayersSelector);
                                } else {
                                    Toast.makeText(ChatActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call <Player> call, Throwable t) {
                                Toast.makeText(ChatActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (spnPlayerType.getSelectedItemId() == 1) {
                        Call <Player> requestPlayer = new RetrofitConfig().getEndpointsService().getPlayerByName(edtPlayerValue.getText().toString());
                        requestPlayer.enqueue(new Callback <Player>() {
                            @Override
                            public void onResponse(Call <Player> call, Response <Player> response) {
                                if (response.isSuccessful()) {
                                    Player player = response.body();

                                    playersSelector.add(player);

                                    adapterPlayersSelector = new AdapterPlayers(ChatActivity.this, playersSelector);
                                    lvPlayerSelector.setAdapter(adapterPlayersSelector);
                                } else {
                                    Toast.makeText(ChatActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call <Player> call, Throwable t) {
                                Toast.makeText(ChatActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else if (spnPlayerType.getSelectedItemId() == 2) {
                        Call <Player> requestPlayer = new RetrofitConfig().getEndpointsService().getPlayerByEmail(edtPlayerValue.getText().toString());
                        requestPlayer.enqueue(new Callback <Player>() {
                            @Override
                            public void onResponse(Call <Player> call, Response <Player> response) {
                                if (response.isSuccessful()) {
                                    Player player = response.body();

                                    playersSelector.add(player);

                                    adapterPlayersSelector = new AdapterPlayers(ChatActivity.this, playersSelector);
                                    lvPlayerSelector.setAdapter(adapterPlayersSelector);
                                } else {
                                    Toast.makeText(ChatActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call <Player> call, Throwable t) {
                                Toast.makeText(ChatActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (spnPlayerType.getSelectedItemId() == 3) {
                        Call <Player> requestPlayer = new RetrofitConfig().getEndpointsService().getPlayerByCity(edtPlayerValue.getText().toString());
                        requestPlayer.enqueue(new Callback <Player>() {
                            @Override
                            public void onResponse(Call <Player> call, Response <Player> response) {
                                if (response.isSuccessful()) {
                                    Player player = response.body();

                                    playersSelector.add(player);

                                    adapterPlayersSelector = new AdapterPlayers(ChatActivity.this, playersSelector);
                                    lvPlayerSelector.setAdapter(adapterPlayersSelector);
                                } else {
                                    Toast.makeText(ChatActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call <Player> call, Throwable t) {
                                Toast.makeText(ChatActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });
        */

        btnSendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Conversation conversation = new Conversation(null, MainActivity.loggedPlayer, playerTalkingTo, edtMessage.getText().toString());
                Call<Conversation> requestCreateConversation = new RetrofitConfig().getEndpointsService().createConversation(conversation);
                requestCreateConversation.enqueue(new Callback<Conversation>() {
                    @Override
                    public void onResponse(Call<Conversation> call, Response<Conversation> response) {
                        if (response.isSuccessful()) {
                            conversations.add(conversation);
                            adapterConversation = new AdapterConversation(activity, conversations);
                            lvConversation.setAdapter(adapterConversation);

                            edtMessage.setText("");
                        } else {
                            Toast.makeText(activity, activity.getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Conversation> call, Throwable t) {
                        Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        tConversation = new Thread() {
            @Override
            public void run() {
                while (!isInterrupted()) {
                    try {
                        Thread.sleep(50000);  //1000ms = 1 sec
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getConversations(playerTalkingTo);
                            }
                        });

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(tConversation.isAlive())
            tConversation.interrupt();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(clConversation.getVisibility() == View.VISIBLE)
            if(tConversation.isAlive())
                tConversation.interrupt();
    }

    private void getPlayers() {
        Call<List<Player>> requestTeams = new RetrofitConfig().getEndpointsService().getPlayersConversationById(MainActivity.loggedPlayer.getId());
        requestTeams.enqueue(new Callback<List<Player>>() {
            @Override
            public void onResponse(Call<List<Player>> call, Response<List<Player>> response) {
                if (response.isSuccessful()) {
                    players = response.body();
                    adapterConversations = new AdapterConversations(ChatActivity.this, players);
                    lvConversations.setAdapter(adapterConversations);
                } else {
                    Toast.makeText(ChatActivity.this, getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Player>> call, Throwable t) {
                Toast.makeText(ChatActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void getConversations(final Player player) {
        Call<List<Conversation>> requestTeams = new RetrofitConfig().getEndpointsService().getConversationById(MainActivity.loggedPlayer.getId(), player.getId());
        requestTeams.enqueue(new Callback<List<Conversation>>() {
            @Override
            public void onResponse(Call<List<Conversation>> call, Response<List<Conversation>> response) {
                if (response.isSuccessful()) {
                    conversations = response.body();
                    adapterConversation = new AdapterConversation(activity, conversations);
                    lvConversation.setAdapter(adapterConversation);


                    activity.setTitle(player.getName().toString());
                    clMainChat.setVisibility(View.GONE);
                    //clPlayerSelector.setVisibility(View.GONE);
                    clConversation.setVisibility(View.VISIBLE);
                    playerTalkingTo = player;

                    if(!tConversation.isAlive())
                        tConversation.start();

                } else {
                    Toast.makeText(activity, activity.getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Conversation>> call, Throwable t) {
                Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
