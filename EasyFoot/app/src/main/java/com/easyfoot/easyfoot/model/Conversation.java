package com.easyfoot.easyfoot.model;

public class Conversation {
    private Long id;
    private Player playerSender;
    private Player playerReceiver;
    private String message;

    public Conversation(){}

    public Conversation(Long id, Player playerSender, Player playerReceiver, String message) {
        this.id = id;
        this.playerSender = playerSender;
        this.playerReceiver = playerReceiver;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Player getPlayerSender() {
        return playerSender;
    }

    public void setPlayerSender(Player playerSender) {
        this.playerSender = playerSender;
    }

    public Player getPlayerReceiver() {
        return playerReceiver;
    }

    public void setPlayerReceiver(Player playerReceiver) {
        this.playerReceiver = playerReceiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
