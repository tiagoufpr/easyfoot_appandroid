package com.easyfoot.easyfoot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Player;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {
    private EditText edtName;
    private EditText edtEmail;
    private EditText edtOldPassword;
    private EditText edtPassword;
    private EditText edtRepeatPassword;
    private Button btnEdit;
    private Button btnCancel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId())
        {
            case android.R.id.home:
                this.finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        edtName = findViewById(R.id.edtProfileName);
        edtEmail = findViewById(R.id.edtProfileEmail);
        edtOldPassword = findViewById(R.id.edtProfileOldPassword);
        edtPassword = findViewById(R.id.edtProfilePassword);
        edtRepeatPassword = findViewById(R.id.edtProfileRepeatPassword);
        btnEdit = findViewById(R.id.btnEditProfile);
        btnCancel = findViewById(R.id.btnCancelProfile);

        edtName.setText(MainActivity.loggedPlayer.getName());
        edtEmail.setText(MainActivity.loggedPlayer.getEmail());

        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    private void edit() {
        if (validate()) {
            MainActivity.loggedPlayer.setName(edtName.getText().toString());

            if (edtPassword.length() > 0)
                MainActivity.loggedPlayer.setPassword(edtPassword.getText().toString());

            Call<Player> playersRequest = new RetrofitConfig().getEndpointsService().updatePlayer(MainActivity.loggedPlayer.getId(), MainActivity.loggedPlayer);
            playersRequest.enqueue(new Callback<Player>() {
                @Override
                public void onResponse(Call<Player> call, Response<Player> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(ProfileActivity.this, getString(R.string.singin_sucess), Toast.LENGTH_SHORT).show();
                        Player player = response.body();
                        MainActivity.tvName.setText(MainActivity.loggedPlayer.getName());
                        MainActivity.tvEmail.setText(MainActivity.loggedPlayer.getEmail());
                        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
                        intent.putExtra("Player", player);
                        startActivity(intent);
                        finish();
                    } else if (!response.isSuccessful()) {
                        Toast.makeText(ProfileActivity.this, getString(R.string.singin_error_emailtaken), Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(ProfileActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<Player> call, Throwable t) {
                    Toast.makeText(ProfileActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean validate() {
        if (edtName.length() == 0) {
            edtName.setError(getString(R.string.error_name));

            if (edtName.isFocusable())
                edtName.requestFocus();

            return false;
        } else if (edtEmail.length() == 0) {
            edtEmail.setError(getString(R.string.error_email));

            if (edtEmail.isFocusable())
                edtEmail.requestFocus();

            return false;
        } else if (edtPassword.length() > 0) {
            if (!(edtOldPassword.getText().toString().equals(MainActivity.loggedPlayer.getPassword()))) {

                edtOldPassword.setError(getString(R.string.profile_error_password));

                if (edtOldPassword.isFocusable())
                    edtOldPassword.requestFocus();

                return false;
            } else if (edtPassword.length() == 0) {
                edtPassword.setError(getString(R.string.error_password));

                if (edtPassword.isFocusable())
                    edtPassword.requestFocus();

                return false;
            } else if (edtRepeatPassword.length() == 0) {
                edtRepeatPassword.setError(getString(R.string.error_password));

                if (edtRepeatPassword.isFocusable())
                    edtRepeatPassword.requestFocus();

                return false;

            } else if (!edtPassword.getText().toString().equals(edtRepeatPassword.getText().toString())) {
                edtPassword.setError(getString(R.string.singin_error_passwordconflict));

                if (edtPassword.isFocusable())
                    edtPassword.requestFocus();

                return false;
            }
        }

        return true;
    }

    public void cancel() {
        Intent intent = new Intent(ProfileActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
