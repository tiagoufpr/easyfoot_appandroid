package com.easyfoot.easyfoot;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Player;

import java.util.List;

public class AdapterPlayers extends BaseAdapter {
    private final Activity activity;
    private final List<Player> players;

    public AdapterPlayers(Activity activity, List<Player> players) {
        this.activity = activity;
        this.players = players;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = activity.getLayoutInflater().inflate(R.layout.adapter_players, parent, false);

        final Player player = players.get(position);

        TextView nome = (TextView) view.findViewById(R.id.adapter_players_name);
        TextView email = (TextView) view.findViewById(R.id.adapter_players_email);
        //ImageView imagem = (ImageView) view.findViewById(R.id.lista_curso_personalizada_imagem);

        nome.setText(player.getName());
        email.setText(player.getEmail());
        //imagem.setImageResource(R.drawable.java);

        Button btnRemovePlayer= (Button) view.findViewById(R.id.btnRemovePlayer);

        btnRemovePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TeamsActivity.removePlayerOfTeam(activity, position);
            }
        });

        LinearLayout llPlayer = (LinearLayout) view.findViewById(R.id.llPlayer);

        if(activity.getClass() == PlayerSearchActivity.class){
            if (PlayerSearchActivity.senderMsg.toString().equals("ChatActivity")) {
                btnRemovePlayer.setVisibility(View.GONE);

                llPlayer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ChatActivity.getConversations(player);
                        activity.finish();
                    }
                });
            } else if (PlayerSearchActivity.senderMsg.toString().equals("TeamsActivity")) {
                btnRemovePlayer.setVisibility(View.VISIBLE);

                llPlayer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TeamsActivity.addPlayerOfTeam(player, activity);
                    }
                });
            }
        }

        return view;
    }
}
