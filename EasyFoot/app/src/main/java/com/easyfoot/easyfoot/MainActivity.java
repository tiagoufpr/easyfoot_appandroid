package com.easyfoot.easyfoot;

/**
 * Created by Tiago Schernoveber on 15/08/2018.
 */

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Game;
import com.easyfoot.easyfoot.model.Player;
import com.easyfoot.easyfoot.model.Team;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  implements NavigationView.OnNavigationItemSelectedListener {
    public static Player loggedPlayer;
    static TextView tvName;
    static TextView tvEmail;
    static TextView tvID;

    public static List<Team> listTeams;
    private static ListView lvTeams;
    private static AdapterTeams adapterTeams;

    public static List<Game> listGames;
    private static ListView lvGames;
    private static AdapterGames adapterGames;

    private ImageView iVIconPartidas, iVIconMeusTimes, iVIconChat, iVIconRanking, iVIconMeusDados, iVIconConfiguracoes, iVIconCompartilhar, iVIconSair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        Intent intent = getIntent();
        loggedPlayer = (Player) intent.getSerializableExtra("Player");

        iVIconPartidas = (ImageView)  findViewById(R.id.iV_Icon_Partidas);
        iVIconMeusTimes = (ImageView)  findViewById(R.id.iV_Icon_MeusTimes);
        iVIconChat = (ImageView)  findViewById(R.id.iV_Icon_Chat);
        iVIconRanking = (ImageView)  findViewById(R.id.iV_Icon_Ranking);
        iVIconMeusDados = (ImageView)  findViewById(R.id.iV_Icon_MeusDados);
        iVIconConfiguracoes = (ImageView)  findViewById(R.id.iV_Icon_Configuracoes);
        iVIconCompartilhar = (ImageView)  findViewById(R.id.iV_Icon_Compartilhar);
        iVIconSair = (ImageView)  findViewById(R.id.iV_Icon_Sair);

        iVIconPartidas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MatchesActivity.class);
                startActivity(intent);
            }
        });

        iVIconMeusTimes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, TeamsActivity.class);
                startActivity(intent);
            }
        });

        iVIconChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ChatActivity.class);
                startActivity(intent);
            }
        });

        iVIconRanking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RankingActivity.class);
                startActivity(intent);
            }
        });

        iVIconMeusDados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        iVIconConfiguracoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ConfigurationsActivity.class);
                startActivity(intent);
            }
        });

        iVIconCompartilhar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                compartilhar();
            }
        });

        iVIconSair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginActivity.autologin = false;
                loggedPlayer = null;
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });



        /*
        lvTeams = (ListView) findViewById(R.id.lvTeams);
        getTeams();

        lvGames = (ListView) findViewById(R.id.lvMatches);
        getGames();
        */


    }

    @Override
    public void onResume() {
        super.onResume();

        //getTeams();
        //getGames();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);

        try {
            tvID = (TextView) findViewById(R.id.tvID);
            tvName = (TextView) findViewById(R.id.tvName);
            tvEmail = (TextView) findViewById(R.id.tvEmail);
            tvID.setText("ID: " + loggedPlayer.getId());
            tvName.setText(loggedPlayer.getName());
            tvEmail.setText(loggedPlayer.getEmail());
        }catch(Exception e){
            System.out.print(e.toString());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, ConfigurationsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent intent = null;

        if (id == R.id.nav_profile)
            intent = new Intent(MainActivity.this, ProfileActivity.class);
        else if (id == R.id.nav_teams)
            intent = new Intent(MainActivity.this, TeamsActivity.class);
        else if (id == R.id.nav_matches)
            intent = new Intent(MainActivity.this, MatchesActivity.class);
        else if (id == R.id.nav_ranking)
            intent = new Intent(MainActivity.this, RankingActivity.class);
        else if (id == R.id.nav_configurations)
            intent = new Intent(MainActivity.this, ConfigurationsActivity.class);
        else if (id == R.id.nav_share)
            compartilhar();
        else if (id == R.id.nav_chat)
            intent = new Intent(MainActivity.this, ChatActivity.class);
        else if (id == R.id.nav_logout) {
            loggedPlayer = null;
            intent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

        if (id != R.id.nav_share)
            startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void compartilhar() {
        try {
            final int REQUEST_EXTERNAL_STORAGE = 1;
            final String[] PERMISSIONS_STORAGE = {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

            int writePermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            int readPermission = ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);

            if (writePermission != PackageManager.PERMISSION_GRANTED || readPermission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(
                        this,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );

                SystemClock.sleep(2500);
            }

            Bitmap bm;


            bm = BitmapFactory.decodeResource(this.getResources(), R.drawable.campanha);


            File imageFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/campanha.png");

            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(imageFile);

                bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);

                fos.close();


            } catch (IOException e) {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }


            Intent share = new Intent(android.content.Intent.ACTION_SEND);
            share.setType("text/html");


            String caminhoFoto = getExternalFilesDir(null) + "/campanha.png";
            File arquivoDaFoto = new File(caminhoFoto);
            //Uri uri = FileProvider.getUriForFile(getBaseContext(), getBaseContext().getApplicationContext().getPackageName() + ".com.easyfoot.easyfoot.provider", arquivoDaFoto);

            share.putExtra(Intent.EXTRA_TEXT, getString(R.string.share_extraText));
            share.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            share.setFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
            //share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, getString(R.string.share_where)));

        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getString(R.string.share_error), Toast.LENGTH_SHORT).show();
        }
    }
}
