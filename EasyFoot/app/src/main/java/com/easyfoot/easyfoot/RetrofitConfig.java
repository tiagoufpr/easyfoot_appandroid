package com.easyfoot.easyfoot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Tiago on 15/09/2018.
 */

public class RetrofitConfig {
    private final Retrofit retrofit;

    public RetrofitConfig(){
        Gson gson = new GsonBuilder()
                .setDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz")
                .create();

        this.retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8080")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    public  EndpointsService getEndpointsService(){
        return this.retrofit.create(EndpointsService.class);
    }
}
