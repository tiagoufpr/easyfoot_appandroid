package com.easyfoot.easyfoot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Game;
import com.easyfoot.easyfoot.model.Place;
import com.easyfoot.easyfoot.model.Registration;
import com.easyfoot.easyfoot.model.Team;
import com.easyfoot.easyfoot.model.TeamRanking;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MatchesActivity extends AppCompatActivity {

    private static Activity activity;

    static ConstraintLayout clMainMatch;
    public static List<Game> listGames;
    private static ListView lvGamesConfirmed;
    private static ListView lvGamesNotConfirmed;
    private static AdapterGames adapterGames;
    private Button btnNewInvite;

    static ConstraintLayout clAddMatch;
    Spinner spnMyTeam;
    public static List<Team> listMyTeams;
    List<String> spnMyTeamsArray;
    ArrayAdapter<String> spnMyTeamsAdapter;

    public static List<Place> listPlaces;
    static Button btnAddTeam;
    static TextView tvSelectedTeam;
    static TextView tvRemoveTeam;
    static Button btnRemoveTeam;

    Team teamInviter;
    static Team teamGuest;

    static EditText edtMatchName;

    Spinner spnPlace;
    List<String> spnPlaceArray;
    ArrayAdapter<String> spnPlaceAdapter;
    Place place;

    EditText edtDate;
    Button btnCreateMatch, btnCancel;

    static LinearLayout llTeamSelector;

    private Spinner spnTeamType;
    private EditText edtTeamValue;
    private Button btnTeamSearch;
    private static ListView lvTeamSelector;
    private static List<Team> teamsSelector;
    private static AdapterTeams adapterTeamsSelector;
    private Button btnTeamSearchCancel;

    static ConstraintLayout clWinnerMatch;

    private Button btnSaveWinner, btnCancelWinner;

    private static TextView txtMyTeamName, txtInvitedTeamName;

    private EditText edtMyGols, edtInvitedGols;

    static Game game;

    static Team teamGameMy, teamGameInvited;

    private TeamRanking teamRanking;

    private RelativeLayout rlDate;

    private CalendarView cvDate;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);

        activity = MatchesActivity.this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        clMainMatch = (ConstraintLayout) findViewById(R.id.clMainMatch);
        lvGamesConfirmed = (ListView) findViewById(R.id.lvGamesConfirmed);
        getGames(true);
        lvGamesNotConfirmed = (ListView) findViewById(R.id.lvGamesNotConfirmed);
        getGames(false);
        btnNewInvite = (Button) findViewById(R.id.btnNewInvite);

        clAddMatch = (ConstraintLayout) findViewById(R.id.clAddMatch);
        spnMyTeam = (Spinner) findViewById(R.id.spnMyTeam);
        spnMyTeamsArray = new ArrayList<>();
        spnMyTeamsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spnMyTeamsArray);
        getMyTeams();
        spnMyTeamsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        btnAddTeam = (Button) findViewById(R.id.btnAddTeam);
        tvSelectedTeam = (TextView) findViewById(R.id.tvSelectedTeam);
        tvRemoveTeam = (TextView) findViewById(R.id.tvRemoveTeam);
        btnRemoveTeam = (Button) findViewById(R.id.btnRemoveTeam);

        edtMatchName = (EditText) findViewById(R.id.edtMatchName);

        spnPlace = (Spinner) findViewById(R.id.spnPlace);
        spnPlaceArray = new ArrayList<>();
        spnPlaceAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, spnPlaceArray);
        getPlaces();
        spnPlaceAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        edtDate = (EditText) findViewById(R.id.edtDate);

        btnCreateMatch = (Button) findViewById(R.id.btnCreateUser);
        btnCancel = (Button) findViewById(R.id.btnCancel);


        spnTeamType = (Spinner) findViewById(R.id.spnTeamType);
        edtTeamValue = (EditText) findViewById(R.id.edtTeamValue);
        btnTeamSearch = (Button) findViewById(R.id.btnTeamSearch);
        lvTeamSelector = (ListView) findViewById(R.id.lvTeamSelector);
        btnTeamSearchCancel = (Button) findViewById(R.id.btnTeamSearchCancel);

        clWinnerMatch = (ConstraintLayout) findViewById(R.id.clWinnerMatch);

        txtMyTeamName = (TextView) findViewById(R.id.txtMyTeamName);
        txtInvitedTeamName = (TextView) findViewById(R.id.txtInvitedTeamName);

        btnCreateMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createMatch();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetMatch();
            }
        });

        llTeamSelector = (LinearLayout) findViewById(R.id.llTeamSelector);

        btnSaveWinner = (Button) findViewById(R.id.btnSaveWinner);
        btnCancelWinner = (Button) findViewById(R.id.btnCancelWinner);

        edtMyGols = (EditText) findViewById(R.id.edtMyGols);
        edtInvitedGols = (EditText) findViewById(R.id.edtInvitedGols);

        rlDate = (RelativeLayout) findViewById(R.id.rlDate);

        cvDate = (CalendarView) findViewById(R.id.cvDate);

        btnRemoveTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                removeTeam();
            }
        });


        spnMyTeam.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                teamInviter = listMyTeams.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                teamInviter = null;
            }

        });

        spnPlace.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                place = listPlaces.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                place = null;
            }
        });

        btnNewInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clMainMatch.setVisibility(View.GONE);
                llTeamSelector.setVisibility(View.GONE);
                clWinnerMatch.setVisibility(View.GONE);
                clAddMatch.setVisibility(View.VISIBLE);
            }
        });

        btnAddTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clMainMatch.setVisibility(View.GONE);
                clAddMatch.setVisibility(View.GONE);
                clWinnerMatch.setVisibility(View.GONE);
                llTeamSelector.setVisibility(View.VISIBLE);
            }
        });


        btnTeamSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtTeamValue.length() == 0) {
                    edtTeamValue.setError(getString(R.string.error_field));

                    if (edtTeamValue.isFocusable())
                        edtTeamValue.requestFocus();
                } else {


                    teamsSelector = new ArrayList<Team>();

                    if (spnTeamType.getSelectedItemId() == 0) {
                        Call<Team> requestPlayer = new RetrofitConfig().getEndpointsService().getTeamById(Long.parseLong(edtTeamValue.getText().toString()));
                        requestPlayer.enqueue(new Callback<Team>() {
                            @Override
                            public void onResponse(Call<Team> call, Response<Team> response) {
                                if (response.isSuccessful()) {
                                    Team team = response.body();

                                    teamsSelector.add(team);

                                    adapterTeamsSelector = new AdapterTeams(MatchesActivity.this, teamsSelector);
                                    lvTeamSelector.setAdapter(adapterTeamsSelector);

                                } else if (!response.isSuccessful()) {
                                    Toast.makeText(MatchesActivity.this, getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                                } else
                                    Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<Team> call, Throwable t) {
                                Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else if (spnTeamType.getSelectedItemId() == 1) {
                        Call<List<Team>> requestPlayer = new RetrofitConfig().getEndpointsService().getTeamByName(edtTeamValue.getText().toString());
                        requestPlayer.enqueue(new Callback<List<Team>>() {
                            @Override
                            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                                if (response.isSuccessful()) {
                                    List<Team> teams = response.body();

                                    //if (teamGuest != null) {

                                    adapterTeamsSelector = new AdapterTeams(MatchesActivity.this, teams);

                                    //teamsSelector.add(team);
                                    //adapterTeamsSelector = new AdapterTeams(MatchesActivity.this, teamsSelector);
                                    lvTeamSelector.setAdapter(adapterTeamsSelector);

                                    //}
                                } else if (!response.isSuccessful()) {
                                    Toast.makeText(MatchesActivity.this, getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                                } else
                                    Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<List<Team>> call, Throwable t) {
                                Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });

                    } else if (spnTeamType.getSelectedItemId() == 2) {
                        Call<List<Team>> requestPlayer = new RetrofitConfig().getEndpointsService().getTeamByCity(edtTeamValue.getText().toString());
                        requestPlayer.enqueue(new Callback<List<Team>>() {
                            @Override
                            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                                if (response.isSuccessful()) {
                                    List<Team> teams = response.body();

                                    adapterTeamsSelector = new AdapterTeams(MatchesActivity.this, teams);

                                    //teamsSelector.add(team);
                                    //adapterTeamsSelector = new AdapterTeams(MatchesActivity.this, teamsSelector);
                                    lvTeamSelector.setAdapter(adapterTeamsSelector);

                                    if (!response.isSuccessful())
                                        Toast.makeText(MatchesActivity.this, getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<List<Team>> call, Throwable t) {
                                Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }
        });

        btnTeamSearchCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtTeamValue.setText("");
                llTeamSelector.setVisibility(View.GONE);
                clMainMatch.setVisibility(View.GONE);
                clWinnerMatch.setVisibility(View.GONE);
                clAddMatch.setVisibility(View.VISIBLE);
            }
        });

        btnSaveWinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setGameResult();
            }
        });

        btnCancelWinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtTeamValue.setText("");
                llTeamSelector.setVisibility(View.GONE);
                clWinnerMatch.setVisibility(View.GONE);
                clAddMatch.setVisibility(View.GONE);
                clMainMatch.setVisibility(View.VISIBLE);
            }
        });

        edtDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    rlDate.setVisibility(View.VISIBLE);
                } else {
                    rlDate.setVisibility(View.GONE);
                }
            }
        });

        edtDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rlDate.setVisibility(View.VISIBLE);
            }
        });

        cvDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                month = month + 1;
                edtDate.setText(dayOfMonth + "/" + month + "/" + year);
                rlDate.setVisibility(View.GONE);
            }
        });
    }

    private static void getGames(final Boolean confirmed) {
        Call<List<Game>> requestGames = new RetrofitConfig().getEndpointsService().findGamesByPlayerAndConfirmed(MainActivity.loggedPlayer.getId(), confirmed);
        requestGames.enqueue(new Callback<List<Game>>() {
            @Override
            public void onResponse(Call<List<Game>> call, Response<List<Game>> response) {
                if (response.isSuccessful()) {
                    List<Game> games = response.body();

                    listGames = games;
                    adapterGames = new AdapterGames(activity, listGames);
                    if (confirmed)
                        lvGamesConfirmed.setAdapter(adapterGames);
                    else
                        lvGamesNotConfirmed.setAdapter(adapterGames);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.matches_error2), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Game>> call, Throwable t) {
                Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMyTeams() {
        Call<List<Team>> requestTeams = new RetrofitConfig().getEndpointsService().getTeamsByPlayer(MainActivity.loggedPlayer.getId());
        requestTeams.enqueue(new Callback<List<Team>>() {
            @Override
            public void onResponse(Call<List<Team>> call, Response<List<Team>> response) {
                if (response.isSuccessful()) {
                    List<Team> teams = response.body();

                    listMyTeams = teams;

                    if (listMyTeams.size() > 0) {
                        spnMyTeamsArray.clear();

                        for (Team team : listMyTeams) {
                            spnMyTeamsArray.add(team.getName().toString());
                        }

                        spnMyTeam.setAdapter(spnMyTeamsAdapter);
                    }

                } else {
                    Toast.makeText(MatchesActivity.this, getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Team>> call, Throwable t) {
                Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getPlaces() {
        Call<List<Place>> requestPlayer = new RetrofitConfig().getEndpointsService().getAllPlaces();
        requestPlayer.enqueue(new Callback<List<Place>>() {
            @Override
            public void onResponse(Call<List<Place>> call, Response<List<Place>> response) {
                if (response.isSuccessful()) {
                    List<Place> places = response.body();

                    listPlaces = places;

                    if (listPlaces.size() > 0) {
                        spnPlaceArray.clear();

                        for (Place place : listPlaces) {
                            spnPlaceArray.add(place.getName().toString());
                        }

                        spnPlace.setAdapter(spnPlaceAdapter);
                    }

                } else {
                    Toast.makeText(MatchesActivity.this, getString(R.string.matches_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<Place>> call, Throwable t) {
                Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void selectTeam(Team team) {
        teamGuest = team;
        btnAddTeam.setVisibility(View.GONE);
        tvSelectedTeam.setText(team.getName().toString());
        tvSelectedTeam.setVisibility(View.VISIBLE);
        tvRemoveTeam.setVisibility(View.VISIBLE);
        btnRemoveTeam.setVisibility(View.VISIBLE);
        clMainMatch.setVisibility(View.GONE);
        llTeamSelector.setVisibility(View.GONE);
        clWinnerMatch.setVisibility(View.GONE);
        clAddMatch.setVisibility(View.VISIBLE);

        if (edtMatchName.hasFocusable())
            edtMatchName.requestFocus();
    }

    private void removeTeam() {
        teamGuest = null;
        tvSelectedTeam.setText("");
        tvSelectedTeam.setVisibility(View.GONE);
        tvRemoveTeam.setVisibility(View.GONE);
        btnRemoveTeam.setVisibility(View.GONE);
        btnAddTeam.setVisibility(View.VISIBLE);
    }

    private void createMatch() {
        if (validateCreate()) {
            List<Team> teams = new ArrayList<>();
            teams.add(teamInviter);
            teams.add(teamGuest);

            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            Date matchDate = null;
            try {
                matchDate = df.parse(edtDate.getText().toString());
            } catch (ParseException e) {
                Toast.makeText(MatchesActivity.this, getString(R.string.matches_error), Toast.LENGTH_SHORT).show();
            }

            Game game = new Game(null, edtMatchName.getText().toString(), place, matchDate, MainActivity.loggedPlayer, teams, false, false);

            //persisto o jogo para poder criar o registro
            createGame(game);
        }
    }

    private void createGame(Game game) {

        Call<Game> gameRequest = new RetrofitConfig().getEndpointsService().createGame(game);
        gameRequest.enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {
                if (response.isSuccessful()) {
                    createRegistration(response.body());
                }
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {

            }
        });
    }

    void createRegistration(Game game) {

        Registration registration = new Registration(null, game, MainActivity.loggedPlayer, teamInviter, null);

        Call<Registration> createMatchRequest = new RetrofitConfig().getEndpointsService().createRegistration(registration);
        createMatchRequest.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(MatchesActivity.this, getString(R.string.matches_sucess), Toast.LENGTH_SHORT).show();
                    resetMatch();
                    getGames(false);
                } else if (!response.isSuccessful()) {
                    Toast.makeText(MatchesActivity.this, getString(R.string.matches_error), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                Toast.makeText(MatchesActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void resetMatch() {
        removeTeam();
        edtMatchName.setText("");
        spnPlace.setSelection(0);
        edtDate.setText("");
        clAddMatch.setVisibility(View.GONE);
        llTeamSelector.setVisibility(View.GONE);
        clWinnerMatch.setVisibility(View.GONE);
        clMainMatch.setVisibility(View.VISIBLE);
    }

    private boolean validateCreate() {
        if ((teamGuest == null) || (teamInviter == null)) {
            Toast.makeText(MatchesActivity.this, getString(R.string.error_team2), Toast.LENGTH_SHORT).show();

            return false;
        } else if (teamInviter == null) {
            Toast.makeText(MatchesActivity.this, getString(R.string.error_team), Toast.LENGTH_SHORT).show();

            return false;
        } else if (edtMatchName.length() == 0) {
            edtMatchName.setError(getString(R.string.error_name));

            if (edtMatchName.isFocusable())
                edtMatchName.requestFocus();

            return false;
        } else if (place == null) {
            Toast.makeText(MatchesActivity.this, getString(R.string.error_place), Toast.LENGTH_SHORT).show();

            return false;
        } else if (edtDate.length() == 0) {
            edtDate.setError(getString(R.string.error_date));

            if (edtDate.isFocusable())
                edtDate.requestFocus();

            return false;
        } else
            return true;
    }

    public static void acceptMatch(final Game game) {
        AlertDialog.Builder dlg;

        dlg = new AlertDialog.Builder(activity);
        dlg.setMessage(activity.getString(R.string.matches_accept_question));
        dlg.setNeutralButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                game.setConfirmed(true);

                Call<Game> updateGameRequest = new RetrofitConfig().getEndpointsService().updateGame(game.getId(), game);
                updateGameRequest.enqueue(new Callback<Game>() {
                    @Override
                    public void onResponse(Call<Game> call, Response<Game> response) {
                        if (response.isSuccessful()) {
                            Toast.makeText(activity, activity.getString(R.string.matches_accept_ok), Toast.LENGTH_SHORT).show();
                            getGames(true);
                            getGames(false);
                        } else if (!response.isSuccessful()) {
                            Toast.makeText(activity, activity.getString(R.string.matches_accept_error), Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }


                    @Override
                    public void onFailure(Call<Game> call, Throwable t) {
                        Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

        dlg.setPositiveButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        dlg.show();
    }

    public static void refuseMatch(final Game game) {
        AlertDialog.Builder dlg;

        dlg = new AlertDialog.Builder(activity);
        dlg.setMessage(activity.getString(R.string.matches_refuse_question));
        dlg.setNeutralButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                checkAndDeleteRegistration(game);
            }
        });

        dlg.setPositiveButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        dlg.show();
    }

    private static void checkAndDeleteRegistration(final Game game) {
        Call<Registration> registrationRequest = new RetrofitConfig().getEndpointsService().getRegistrationByGameId(game.getId());
        registrationRequest.enqueue(new Callback<Registration>() {
            @Override
            public void onResponse(Call<Registration> call, Response<Registration> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null)
                        deleteRegistration(response.body());

                    deleteGame(game);
                } else if (!response.isSuccessful()) {
                    Toast.makeText(activity, activity.getString(R.string.matches_refuse_error), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Registration> call, Throwable t) {
                Toast.makeText(activity, "Erro ao encontrar o registro", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public static void deleteGame(Game game) {
        Call<ResponseBody> deleteGameRequest = new RetrofitConfig().getEndpointsService().deleteGame(game.getId());
        deleteGameRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(activity, activity.getString(R.string.matches_refuse_ok), Toast.LENGTH_SHORT).show();
                    getGames(true);
                    getGames(false);
                } else if (!response.isSuccessful()) {
                    Toast.makeText(activity, activity.getString(R.string.matches_refuse_error), Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public static void deleteRegistration(Registration registration) {
        Call<ResponseBody> registrationRequest = new RetrofitConfig().getEndpointsService().deleteRegistrationById(registration.getId());
        registrationRequest.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(activity, "Registro deletado com sucesso", Toast.LENGTH_SHORT).show();
                    } else if (!response.isSuccessful()) {
                        Toast.makeText(activity, activity.getString(R.string.matches_accept_error), Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity, "Erro ao deletar o Registro", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setGameResult() {
        if (edtMyGols.length() == 0) {
            edtMyGols.setError(getString(R.string.error_gols));

            if (edtMyGols.isFocusable())
                edtMyGols.requestFocus();

        } else if (edtInvitedGols.length() == 0) {
            edtInvitedGols.setError(getString(R.string.error_gols));

            if (edtInvitedGols.isFocusable())
                edtInvitedGols.requestFocus();

        } else {
            doTeamRanking(teamGameMy, Double.parseDouble(edtMyGols.getText().toString()));
            doTeamRanking(teamGameInvited, Double.parseDouble(edtInvitedGols.getText().toString()));

            game.setFinished(true);
            updateGame();

            clAddMatch.setVisibility(View.GONE);
            llTeamSelector.setVisibility(View.GONE);
            clWinnerMatch.setVisibility(View.GONE);
            clMainMatch.setVisibility(View.VISIBLE);
        }
    }

    private void doTeamRanking(final Team team, final Double points) {

        Call<TeamRanking> registrationRequest = new RetrofitConfig().getEndpointsService().getTeamRankingById(team.getId());
        registrationRequest.enqueue(new Callback<TeamRanking>() {
            @Override
            public void onResponse(Call<TeamRanking> call, Response<TeamRanking> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        teamRanking = response.body();
                        teamRanking.setPoints(teamRanking.getPoints() + points);

                        Call<TeamRanking> registrationRequest = new RetrofitConfig().getEndpointsService().updateTeamRanking(teamRanking.getId(), teamRanking);
                        registrationRequest.enqueue(new Callback<TeamRanking>() {
                            @Override
                            public void onResponse(Call<TeamRanking> call, Response<TeamRanking> response) {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        Toast.makeText(activity, "Resultado gravado com sucesso!", Toast.LENGTH_SHORT).show();
                                    } else if (!response.isSuccessful()) {
                                        Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                                    } else
                                        Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<TeamRanking> call, Throwable t) {
                                Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }
            }


            @Override
            public void onFailure(Call<TeamRanking> call, Throwable t) {
                teamRanking = new TeamRanking();
                teamRanking.setPoints(points);
                teamRanking.setTeam(team);

                Call<ResponseBody> registrationRequest = new RetrofitConfig().getEndpointsService().createTeamRanking(teamRanking);
                registrationRequest.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                Toast.makeText(activity, "Resultado gravado com sucesso!", Toast.LENGTH_SHORT).show();
                            } else if (!response.isSuccessful()) {
                                Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

    private void updateGame() {
        Call<Game> registrationRequest = new RetrofitConfig().getEndpointsService().updateGame(game.getId(), game);
        registrationRequest.enqueue(new Callback<Game>() {
            @Override
            public void onResponse(Call<Game> call, Response<Game> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Toast.makeText(activity, "Resultado gravado com sucesso!", Toast.LENGTH_SHORT).show();
                        getGames(true);
                        getGames(false);
                    } else if (!response.isSuccessful()) {
                        Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Game> call, Throwable t) {
                Toast.makeText(activity, "Erro ao gravar resultado!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    static void startSetResult(Team game1, Team game2) {
        teamGameMy = game1;
        teamGameInvited = game2;

        txtMyTeamName.setText(teamGameMy.getName());
        txtInvitedTeamName.setText(teamGameInvited.getName());

        clAddMatch.setVisibility(View.GONE);
        llTeamSelector.setVisibility(View.GONE);
        clMainMatch.setVisibility(View.GONE);
        clWinnerMatch.setVisibility(View.VISIBLE);
    }
}
