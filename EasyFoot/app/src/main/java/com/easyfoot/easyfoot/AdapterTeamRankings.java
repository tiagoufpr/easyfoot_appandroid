package com.easyfoot.easyfoot;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.easyfoot.easyfoot.model.TeamRanking;
import java.util.List;

public class AdapterTeamRankings extends BaseAdapter {
    private final Activity activity;
    private final List<TeamRanking> teamRankings;

    public AdapterTeamRankings(Activity activity, List<TeamRanking> teamRankings) {
        this.activity = activity;
        this.teamRankings = teamRankings;
    }

    @Override
    public int getCount() {
        return teamRankings.size();
    }

    @Override
    public Object getItem(int position) {
        return teamRankings.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = activity.getLayoutInflater().inflate(R.layout.adapter_team_rankings, parent, false);

        final TeamRanking teamRanking = teamRankings.get(position);

        TextView name = (TextView) view.findViewById(R.id.adapter_team_name);
        TextView capitain = (TextView) view.findViewById(R.id.adapter_team_captain);
        TextView adapter_team_points = (TextView) view.findViewById(R.id.adapter_team_points);
        //ImageView imagem = (ImageView) view.findViewById(R.id.lista_curso_personalizada_imagem);

        name.setText(activity.getString(R.string.ranking_team) +" " +teamRanking.getTeam().getName().toString());
        capitain.setText(activity.getString(R.string.ranking_captain) +" " +teamRanking.getTeam().getCaptain().getName().toString());
        adapter_team_points.setText(activity.getString(R.string.ranking_points) +" " + String.valueOf(teamRanking.getPoints()));
        //imagem.setImageResource(R.drawable.java);

        LinearLayout llTeam = (LinearLayout) view.findViewById(R.id.llTeam);

        return view;
    }
}
