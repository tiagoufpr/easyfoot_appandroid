package com.easyfoot.easyfoot;

import com.easyfoot.easyfoot.model.Conversation;
import com.easyfoot.easyfoot.model.Game;
import com.easyfoot.easyfoot.model.Place;
import com.easyfoot.easyfoot.model.Player;
import com.easyfoot.easyfoot.model.Registration;
import com.easyfoot.easyfoot.model.Team;
import com.easyfoot.easyfoot.model.TeamRanking;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Tiago.
 */

public interface EndpointsService {

    @POST("auth/login")
    Call<Player> login(@Query("email") String email, @Query("password") String password);

    @POST("auth/loginsocial")
    Call<Player> loginSocial(@Query("email") String email);

    // Player Endpoints
    @GET("player/getAll")
    Call<List<Player>> getAllPlayers();

    @POST("player/create")
    Call<Player> createPlayer(@Body Player player);

    @GET("player/get/{id}")
    Call<Player> getPlayerById(@Path("id") Long id);

    @GET("player/getbyname/{name}")
    Call<List<Player>> getPlayerByName(@Path("name") String name);

    @GET("player/getbyemail/{email}")
    Call<Player> getPlayerByEmail(@Path("email") String email);

    @GET("player/getbyemails/{email}")
    Call<List<Player>> getPlayerByEmails(@Path("email") String email);

    @GET("player/getbycity/{city}")
    Call<Player> getPlayerByCity(@Path("city") String city);

    @PUT("player/update/{id}")
    Call<Player> updatePlayer(@Path("id") Long id, @Body Player player);

    @DELETE("player/delete/{id}")
    Call<Player> deletePlayer(@Path("id") Long id, @Body Player player);



    // Team Endpoints
    @GET("team/getAll")
    Call<List<Team>> getAllTeams();

    @GET("team/get/byPlayer/{id}")
    Call<List<Team>> getTeamsByPlayer(@Path("id") Long id);

    @POST("team/create")
    Call<Team> createTeam(@Body Team team);

    @GET("team/get/{id}")
    Call<Team> getTeamById(@Path("id") Long id);

    @GET("team/getbyname/{name}")
    Call<List<Team>> getTeamByName(@Path("name") String name);

    @GET("team/getbycity/{city}")
    Call<List<Team>> getTeamByCity(@Path("city") String city);

    @PUT("team/update/{id}")
    Call<Team> updateTeam(@Path("id") Long id, @Body Team team);

    @DELETE("team/delete/{id}")
    Call<ResponseBody> deleteTeam(@Path("id") Long id);


    // Registration Endpoints
    @GET("registration/getAll")
    Call<List<Registration>> getAllRegistrations();

    @POST("registration/create")
    Call<Registration> createRegistration(@Body Registration registration);

    @GET("registration/get/byGame/{id}")
    Call<Registration> getRegistrationByGameId(@Path("id")Long id);

    @DELETE("registration/delete/{id}")
    Call<ResponseBody> deleteRegistrationById(@Path("id")Long id);

    //Games Endpoints
    @GET("game/get/byPlayer/{id}")
    Call<List<Game>> findGamesByPlayer(@Path("id") Long id);

    @GET("game/get/findGamesByPlayerAndConfirmed/{id}&{confirmed}")
    Call<List<Game>> findGamesByPlayerAndConfirmed(@Path("id") Long id, @Path("confirmed") Boolean confirmed);

    @POST("game/create")
    Call<Game> createGame(@Body Game game);

    //Places Endpoints
    @GET("place/getAll")
    Call<List<Place>> getAllPlaces();

    @PUT("game/update/{id}")
    Call<Game> updateGame(@Path("id") Long id, @Body Game gameDetails);

    @DELETE("game/delete/{id}")
    Call<ResponseBody> deleteGame(@Path("id") Long id);



    //Ranking Endpoints
    @GET("ranking/team/getAll")
    Call<List<TeamRanking>> findTeamRankings();

    @GET("ranking/team/get/{id}")
    Call<TeamRanking> getTeamRankingById(@Path("id") Long id);

    @POST("ranking/team/create")
    Call<ResponseBody> createTeamRanking(@Body TeamRanking teamRanking);

    @PUT("ranking/team/update/{id}")
    Call<TeamRanking> updateTeamRanking(@Path("id") Long id, @Body TeamRanking teamRankingDetails);



    //Chat Endpoints
    @POST("conversation/create")
    Call<Conversation> createConversation(@Body Conversation conversation);

    @GET("conversation/getPlayers/{id}")
    Call<List<Player>> getPlayersConversationById(@Path("id") Long idSender);

    @GET("conversation/get/{idsender}&{idreceiver}")
    Call<List<Conversation>> getConversationById(@Path("idsender") Long idSender, @Path("idreceiver") Long idReceiver);
}
