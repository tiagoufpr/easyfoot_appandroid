package com.easyfoot.easyfoot;

/**
 * Created by Tiago Schernoveber on 15/08/2018.
 */

import java.util.Timer;
import java.util.TimerTask;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Build;

public class SplashScreenActivity extends Activity {
    Intent intent;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().setNavigationBarColor(getResources().getColor(com.easyfoot.easyfoot.R.color.colorBack));
            getWindow().setStatusBarColor(getResources().getColor(com.easyfoot.easyfoot.R.color.colorBarSplash));
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                intent = new Intent();

                intent.setClass(SplashScreenActivity.this, LoginActivity.class);
                startActivity(intent);

                finish();
            }
        }, 1500);
    }
}
