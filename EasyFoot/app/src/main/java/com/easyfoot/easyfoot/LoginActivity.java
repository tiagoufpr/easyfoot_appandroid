package com.easyfoot.easyfoot;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Player;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;


import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {
    public static Boolean autologin = true;
    private DataBase dataBase;
    private SQLiteDatabase conn;
    private Cursor cursor;

    String sqlConsultaConfiguracoes = "SELECT * FROM CONFIGURATIONS";

    private EditText eEmail;
    private EditText ePassword;
    GoogleSignInClient mGoogleSignInClient;
    int RC_SIGN_IN = 0;

    private ImageView iVFacebook, iVGoogle;
    boolean logginGoogle = false;
    boolean logginFacebook = false;
    CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        eEmail = findViewById(R.id.edtEmail);
        ePassword = findViewById(R.id.edtPassword);

        Button bLogin = findViewById(R.id.btnLogin);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate())
                    login();
            }
        });

        Button btnSingIn = findViewById(R.id.btnCreateUser);

        btnSingIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SingInActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iVFacebook = (ImageView) findViewById(R.id.iVFacebook);

        iVFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInFacebook();
            }
        });

        iVGoogle = (ImageView) findViewById(R.id.iVGoogle);

        iVGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoogle();
            }
        });

        loginSaved();

        //logOutFacebook();
    }

    private void loginSaved() {
        try {
            dataBase = new DataBase(this);
            conn = dataBase.getWritableDatabase();

            cursor = conn.rawQuery(sqlConsultaConfiguracoes, null);


            if (cursor.getCount() > 0) {

                cursor.moveToFirst();

                if ((autologin) && (cursor.getString(cursor.getColumnIndex("loginSave")).trim().equals("S"))) {
                    eEmail.setText(cursor.getString(cursor.getColumnIndex("loginEmail")));
                    ePassword.setText(cursor.getString(cursor.getColumnIndex("loginPassword")));

                    login();
                }
            }
        } catch (SQLException ex) {
            Toast.makeText(getApplicationContext(), com.easyfoot.easyfoot.R.string.configurations_error_load, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validate() {
        if (eEmail.length() == 0) {
            eEmail.setError(getString(R.string.error_email));

            if (eEmail.isFocusable())
                eEmail.requestFocus();

            return false;
        } else if (ePassword.length() == 0) {
            ePassword.setError(getString(R.string.error_password));

            if (ePassword.isFocusable())
                ePassword.requestFocus();

            return false;
        } else
            return true;
    }

    private void login() {
        Call<Player> requestPlayer = new RetrofitConfig().getEndpointsService().login(eEmail.getText().toString(), ePassword.getText().toString());
        requestPlayer.enqueue(new Callback<Player>() {
            @Override
            public void onResponse(Call<Player> call, Response<Player> response) {
                if (response.isSuccessful()) {
                    Player player = response.body();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Player", player);
                    startActivity(intent);
                    //finish();
                } else {
                    Toast.makeText(LoginActivity.this, getString(R.string.login_error), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Player> call, Throwable t) {
                Toast.makeText(LoginActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void signInGoogle() {
        logginGoogle = true;

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mGoogleSignInClient.signOut();

        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if ((logginGoogle) && (requestCode == RC_SIGN_IN)) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else if (logginFacebook) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            loginSocial(account.getEmail(), account.getDisplayName(), account.getId());
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Toast.makeText(LoginActivity.this, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onStart() {
        if (logginGoogle) {
            // Check for existing Google Sign In account, if the user is already signed in
            // the GoogleSignInAccount will be non-null.
            GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
            if (account != null) {
                loginSocial(account.getEmail(), account.getDisplayName(), account.getId());
            }
        }

        super.onStart();
    }

    private void signInFacebook() {
        logginFacebook = true;

        //Login Facebook
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Profile profile = Profile.getCurrentProfile();
                        loginSocial(profile.getFirstName()+ profile.getLastName() + "@facebook.com", profile.getName(), "2166243990348447");
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Toast.makeText(LoginActivity.this, "Erro logar com Facebook!", Toast.LENGTH_SHORT).show();
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    public void logOutFacebook() {

        if (AccessToken.getCurrentAccessToken() != null) {

            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                    .Callback() {
                @Override
                public void onCompleted(GraphResponse graphResponse) {

                    LoginManager.getInstance().logOut();

                }
            }).executeAsync();
        }
    }

    private void loginSocial(final String email, final String name, final String id) {
        Call<Player> requestPlayer = new RetrofitConfig().getEndpointsService().loginSocial(email);
        requestPlayer.enqueue(new Callback<Player>() {
            @Override
            public void onResponse(Call<Player> call, Response<Player> response) {
                if (response.isSuccessful()) {
                    Player player = response.body();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("Player", player);
                    startActivity(intent);
                    finish();
                } else {
                    Player player = new Player();
                    player.setName(name);
                    player.setEmail(email);
                    player.setPassword(id);

                    Call<Player> playersRequest = new RetrofitConfig().getEndpointsService().createPlayer(player);
                    playersRequest.enqueue(new Callback<Player>() {
                        @Override
                        public void onResponse(Call<Player> call, Response<Player> response) {
                            if (response.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, getString(R.string.singin_sucess), Toast.LENGTH_SHORT).show();
                                Player player = response.body();
                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                intent.putExtra("Player", player);
                                startActivity(intent);
                                finish();
                            } else
                                Toast.makeText(LoginActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onFailure(Call<Player> call, Throwable t) {
                            Toast.makeText(LoginActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<Player> call, Throwable t) {
                Toast.makeText(LoginActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
    //Descobrir HasKey
    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }
    */
}


