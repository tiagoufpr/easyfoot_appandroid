package com.easyfoot.easyfoot;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.easyfoot.easyfoot.model.Player;
import java.util.List;

public class AdapterConversations extends BaseAdapter {
    private final Activity activity;
    private final List<Player> players;

    public AdapterConversations(Activity activity, List<Player> players) {
        this.activity = activity;
        this.players = players;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.adapter_conversations, parent, false);

        final Player player = players.get(position);

        TextView tvPlayerConversation = (TextView) view.findViewById(R.id.tvPlayerConversation);
        tvPlayerConversation.setText(player.getName().toString());

        FrameLayout llPlayerConversation = (FrameLayout) view.findViewById(R.id.llPlayerConversation);

        llPlayerConversation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatActivity.getConversations(player);
            }
        });

        return view;
    }
}

