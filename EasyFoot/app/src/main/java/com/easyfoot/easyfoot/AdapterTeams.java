package com.easyfoot.easyfoot;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.easyfoot.easyfoot.model.Player;
import com.easyfoot.easyfoot.model.Team;

import java.util.List;

public class AdapterTeams extends BaseAdapter {
    private final Activity activity;
    private final List<Team> teams;

    public AdapterTeams(Activity activity, List<Team> teams) {
        this.activity = activity;
        this.teams = teams;
    }

    @Override
    public int getCount() {
        return teams.size();
    }

    @Override
    public Object getItem(int position) {
        return teams.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = activity.getLayoutInflater().inflate(R.layout.adapter_teams, parent, false);

        final Team team = teams.get(position);

        TextView name = (TextView) view.findViewById(R.id.adapter_team_name);
        TextView capitain = (TextView) view.findViewById(R.id.adapter_team_captain);
        TextView coCapitain = (TextView) view.findViewById(R.id.adapter_team_cocaptain);
        //ImageView imagem = (ImageView) view.findViewById(R.id.lista_curso_personalizada_imagem);

        name.setText(team.getName().toString());
        capitain.setText(team.getCaptain().getName().toString());
        coCapitain.setText(team.getCoCaptain().getName().toString());
        //imagem.setImageResource(R.drawable.java);

        LinearLayout llTeam = (LinearLayout) view.findViewById(R.id.llTeam);

        Button btnRemoveTeam= (Button) view.findViewById(R.id.btnRemoveTeam);

        if(activity.getClass() == MainActivity.class)
            btnRemoveTeam.setVisibility(View.GONE);
        else
            btnRemoveTeam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TeamsActivity.removeTeam(activity, team.getId());
                }
            });

        llTeam = (LinearLayout) view.findViewById(R.id.llTeam);

        if (activity.getClass() == MatchesActivity.class) {
            btnRemoveTeam.setVisibility(View.GONE);

            llTeam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MatchesActivity.selectTeam(team);
                }
            });
        } else if (activity.getClass() == TeamsActivity.class) {
            llTeam.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TeamsActivity.enableUpdateTeam(activity, position);
                }
            });
        }

        return view;
    }
}

