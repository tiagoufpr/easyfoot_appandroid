package com.easyfoot.easyfoot;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Team;
import com.easyfoot.easyfoot.model.TeamRanking;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RankingActivity extends AppCompatActivity {

    private static Activity activity;

    static ListView lvRanking;
    public static List<TeamRanking> listTeamRanking;
    private static AdapterTeamRankings adapterTeamReankings;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId())
        {
            case android.R.id.home:
                this.finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        activity = RankingActivity.this;

        lvRanking = (ListView) findViewById(R.id.lvRanking);

        getRanking();
    }

    private void getRanking(){
        Call<List<TeamRanking>> requestRanking = new RetrofitConfig().getEndpointsService().findTeamRankings();
        requestRanking.enqueue(new Callback<List<TeamRanking>>() {
            @Override
            public void onResponse(Call<List<TeamRanking>> call, Response<List<TeamRanking>> response) {
                if (response.isSuccessful()) {
                    List<TeamRanking> teamsRankings = response.body();

                    listTeamRanking = teamsRankings;
                    adapterTeamReankings = new AdapterTeamRankings(activity, listTeamRanking);
                    lvRanking.setAdapter(adapterTeamReankings);
                } else {
                    Toast.makeText(RankingActivity.this, getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<TeamRanking>> call, Throwable t) {
                Toast.makeText(activity, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
