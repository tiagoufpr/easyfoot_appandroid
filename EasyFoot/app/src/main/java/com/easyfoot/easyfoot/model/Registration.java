package com.easyfoot.easyfoot.model;

import java.util.Date;

public class Registration {

    private Long id;
    private Game game;
    private Player player;
    private Team team;
    private Date registerDate;

    public Registration(){}

    public Registration(Long id, Game game, Player player, Team team, Date registerDate) {
        this.id = id;
        this.game = game;
        this.player = player;
        this.team = team;
        this.registerDate = registerDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Date getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(Date registerDate) {
        this.registerDate = registerDate;
    }
}
