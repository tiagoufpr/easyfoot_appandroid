package com.easyfoot.easyfoot.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Tiago.
 */

public class Team {

    private Long id;
    private String name;
    private City city;
    private Set<Player> players = new HashSet<>();
    //private Set<Game> games = new HashSet<>();
    private Player captain;
    private Player coCaptain;

    public Team(){}

    public Team(Long id, String name, City city, Set<Player> players/*,Set<Game> games*/, Player captain, Player coCaptain) {
        this.id = id;
        this.name = name;
        this.city = city;
        this.players = players;
        //this.games = games;
        this.captain = captain;
        this.coCaptain = coCaptain;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Set<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Set<Player> players) {
        this.players = players;
    }

    public Player getCaptain() {
        return captain;
    }

    public void setCaptain(Player captain) {
        this.captain = captain;
    }

    public Player getCoCaptain() {
        return coCaptain;
    }

    public void setCoCaptain(Player coCaptain) {
        this.coCaptain = coCaptain;
    }

    //public Set<Game> getGames() {
    //    return games;
    //}

    //public void setGames(Set<Game> games) {
    //    this.games = games;
    //}
}
