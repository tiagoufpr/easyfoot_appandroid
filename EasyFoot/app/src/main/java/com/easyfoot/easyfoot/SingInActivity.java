package com.easyfoot.easyfoot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Player;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SingInActivity extends AppCompatActivity {
    private EditText eName;
    private EditText eEmail;
    private EditText ePassword;
    private EditText eRepeatPassword;
    private Button btnSingIn;
    private Button btnCancel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        eName = findViewById(R.id.edtIDTeam);
        eEmail = findViewById(R.id.edtEmail);
        ePassword = findViewById(R.id.edtPassword);
        eRepeatPassword = findViewById(R.id.edtRepeatPassword);
        btnSingIn = findViewById(R.id.btnCreateUser);
        btnCancel = findViewById(R.id.btnCancel);


        btnSingIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singIn();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancel();
            }
        });
    }

    private void singIn() {
        if(validate()) {
            Player player = new Player();
            player.setName(eName.getText().toString());
            player.setEmail(eEmail.getText().toString());
            player.setPassword(ePassword.getText().toString());

            Call<Player> playersRequest = new RetrofitConfig().getEndpointsService().createPlayer(player);
            playersRequest.enqueue(new Callback<Player>() {
                @Override
                public void onResponse(Call<Player> call, Response<Player> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(SingInActivity.this, getString(R.string.singin_sucess), Toast.LENGTH_SHORT).show();
                        Player player = response.body();
                        Intent intent = new Intent(SingInActivity.this, MainActivity.class);
                        intent.putExtra("Player", player);
                        startActivity(intent);
                        finish();
                    } else if(!response.isSuccessful()){
                        Toast.makeText(SingInActivity.this, getString(R.string.singin_error_emailtaken), Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(SingInActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onFailure(Call<Player> call, Throwable t) {
                    Toast.makeText(SingInActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private boolean validate() {
        if (eName.length() == 0) {
            eName.setError(getString(R.string.error_name));

            if (eName.isFocusable())
                eName.requestFocus();

            return false;
        } else if (eEmail.length() == 0) {
            eEmail.setError(getString(R.string.error_email));

            if (eEmail.isFocusable())
                eEmail.requestFocus();

            return false;
        } else if (ePassword.length() == 0) {
            ePassword.setError(getString(R.string.error_password));

            if (ePassword.isFocusable())
                ePassword.requestFocus();

            return false;
        } else if (eRepeatPassword.length() == 0) {
            eRepeatPassword.setError(getString(R.string.error_password));

            if (eRepeatPassword.isFocusable())
                eRepeatPassword.requestFocus();

            return false;
        } else if (!ePassword.getText().toString().equals(eRepeatPassword.getText().toString())) {
            ePassword.setError(getString(R.string.singin_error_passwordconflict));

            if (ePassword.isFocusable())
                ePassword.requestFocus();

            return false;
        } else
            return true;
    }

    public void cancel() {
        Intent intent = new Intent(SingInActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
