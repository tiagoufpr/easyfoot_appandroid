package com.easyfoot.easyfoot.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Game {

    private Long id;
    private String name;
    private Place place;
    private Date dateTime;
    private Player createdBy;
    private List<Team> teams = new ArrayList<>();

    private Boolean confirmed;

    private Boolean finished;

    public Game() {}

    public Game(Long id, String name, Place place, Date dateTime, Player createdBy, List<Team> teams, Boolean confirmed, Boolean finished) {
        this.id = id;
        this.name = name;
        this.place = place;
        this.dateTime = dateTime;
        this.createdBy = createdBy;
        this.teams = teams;
        this.confirmed = confirmed;
        this.finished = finished;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public Player getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Player createdBy) {
        this.createdBy = createdBy;
    }

    public List<Team> getTeams() {
        return teams;
    }

    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }
}
