package com.easyfoot.easyfoot;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.Player;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlayerSearchActivity extends AppCompatActivity {

    private Spinner spnSearchType;
    private Call<Player> requestPlayer;
    private Call<List<Player>> requestPlayers;
    private EditText edtPlayerSearch;
    private Button btnSearch;

    public static List<Player> listPlayers;
    private static ListView lvPlayers;
    private static AdapterPlayers adapterPlayers;

    public static String senderMsg;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_search);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (getIntent() != null && getIntent().getExtras() != null) {
            Intent sender = getIntent();
            senderMsg = sender.getExtras().getString("sender");
        }

        edtPlayerSearch = (EditText) findViewById(R.id.edtPlayerSearch);
        spnSearchType = (Spinner) findViewById(R.id.spnSearchType);

        btnSearch = (Button) findViewById(R.id.btnSearch);

        List<String> list = new ArrayList<>();
        list.add("ID");
        list.add("Nome");
        list.add("E-mail");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnSearchType.setAdapter(dataAdapter);

        listPlayers = new ArrayList <>();

        lvPlayers = (ListView) findViewById(R.id.lvPlayers);

        spnSearchType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                // your code here
                if(spnSearchType.getSelectedItemId()==0)
                    edtPlayerSearch.setInputType(InputType.TYPE_CLASS_NUMBER);
                else
                    edtPlayerSearch.setInputType(InputType.TYPE_CLASS_TEXT);

                edtPlayerSearch.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchPlayer();
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();

        senderMsg = "";
    }


    public void searchPlayer() {
        if (edtPlayerSearch.length() == 0) {
            edtPlayerSearch.setError(getString(R.string.error_field));

            if (edtPlayerSearch.isFocusable())
                edtPlayerSearch.requestFocus();
        }else {
            if (spnSearchType.getSelectedItemId() == 0) {
                requestPlayer = new RetrofitConfig().getEndpointsService().getPlayerById(Long.parseLong(edtPlayerSearch.getText().toString()));

                requestPlayer.enqueue(new Callback <Player>() {
                    @Override
                    public void onResponse(Call <Player> call, Response <Player> response) {
                        if (response.isSuccessful()) {
                            Player player = response.body();
                            listPlayers.clear();
                            listPlayers.add(player);

                            adapterPlayers = new AdapterPlayers(PlayerSearchActivity.this, listPlayers);
                            lvPlayers.setAdapter(adapterPlayers);
                        } else {
                            Toast.makeText(PlayerSearchActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call <Player> call, Throwable t) {
                        Toast.makeText(PlayerSearchActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }
                });

            } else if (spnSearchType.getSelectedItemId() == 1){
                requestPlayers = new RetrofitConfig().getEndpointsService().getPlayerByName(edtPlayerSearch.getText().toString());

                Call<List<Player>> requestTeams = new RetrofitConfig().getEndpointsService().getPlayerByName(edtPlayerSearch.getText().toString());
                requestTeams.enqueue(new Callback<List<Player>>() {
                    @Override
                    public void onResponse(Call<List<Player>> call, Response<List<Player>> response) {
                        if (response.isSuccessful()) {
                            List<Player> players = response.body();

                            listPlayers = players;
                            adapterPlayers = new AdapterPlayers(PlayerSearchActivity.this, listPlayers);
                            lvPlayers.setAdapter(adapterPlayers);
                        } else {
                            Toast.makeText(PlayerSearchActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Player>> call, Throwable t) {
                        Toast.makeText(PlayerSearchActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }
                });
            } else if (spnSearchType.getSelectedItemId() == 2){
                requestPlayers = new RetrofitConfig().getEndpointsService().getPlayerByEmails(edtPlayerSearch.getText().toString());

                Call<List<Player>> requestTeams = new RetrofitConfig().getEndpointsService().getPlayerByEmails(edtPlayerSearch.getText().toString());
                requestTeams.enqueue(new Callback<List<Player>>() {
                    @Override
                    public void onResponse(Call<List<Player>> call, Response<List<Player>> response) {
                        if (response.isSuccessful()) {
                            List<Player> players = response.body();

                            listPlayers = players;
                            adapterPlayers = new AdapterPlayers(PlayerSearchActivity.this, listPlayers);
                            lvPlayers.setAdapter(adapterPlayers);
                        } else {
                            Toast.makeText(PlayerSearchActivity.this, getString(R.string.team_erro_player), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Player>> call, Throwable t) {
                        Toast.makeText(PlayerSearchActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}
