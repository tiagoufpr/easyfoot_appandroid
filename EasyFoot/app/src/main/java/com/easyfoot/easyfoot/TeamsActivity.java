package com.easyfoot.easyfoot;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.easyfoot.easyfoot.model.City;
import com.easyfoot.easyfoot.model.Country;
import com.easyfoot.easyfoot.model.Player;
import com.easyfoot.easyfoot.model.State;
import com.easyfoot.easyfoot.model.Team;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamsActivity extends AppCompatActivity {
    private Intent intent;

    private static ConstraintLayout clMainTeam;

    public static List <Team> listTeams;
    private static ListView lvTeams;
    private static AdapterTeams adapterTeams;

    private static ScrollView clAddTeam;
    private Button btnAddNewTeam;
    private EditText edtName;
    private Spinner spnCity;
    public static List <Player> listPlayers;
    private static ListView lvPlayers;
    private static AdapterPlayers adapterPlayers;
    private Button btnAddPlayer;
    private Button btnCreateTeam;
    private Button btnCancel;

    private static ScrollView clUpdateTeam;
    private static Team updatingTeam;
    private static EditText edtUpdateName;
    private Spinner spnUpdateCity;
    public static List <Player> listUpdatePlayers;
    private static ListView lvUpdatePlayers;
    private static AdapterPlayers adapterUpdatePlayers;
    private Button btnAddUpdatePlayer;
    private Button btnUpdateTeam;
    private Button btnUpdateCancel;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return (true);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Main
        intent = new Intent();

        clMainTeam = (ConstraintLayout) findViewById(R.id.clMainTeam);
        btnAddNewTeam = (Button) findViewById(R.id.btnAddNewTeam);

        lvTeams = (ListView) findViewById(R.id.lvMatches);
        getTeams(TeamsActivity.this);


        //Add Team
        clAddTeam = (ScrollView) findViewById(R.id.clAddTeam);

        edtName = (EditText) findViewById(R.id.edtIDTeam);
        spnCity = (Spinner) findViewById(R.id.spnCity);
        lvPlayers = (ListView) findViewById(R.id.lvPlayers);
        listPlayers = getPlayersOfTeam();
        btnAddPlayer = (Button) findViewById(R.id.btnAddTeam);
        btnAddUpdatePlayer = (Button) findViewById(R.id.btnUpdateAddPlayer);
        adapterPlayers = new AdapterPlayers(this, listPlayers);
        lvPlayers.setAdapter(adapterPlayers);
        btnCreateTeam = (Button) findViewById(R.id.btnCreateTeam);
        btnCancel = (Button) findViewById(R.id.btnCancel);

        btnAddPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("sender", "TeamsActivity");

                intent.setClass(TeamsActivity.this, PlayerSearchActivity.class);
                startActivity(intent);
            }
        });

        btnAddUpdatePlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent.putExtra("sender", "TeamsActivity");

                intent.setClass(TeamsActivity.this, PlayerSearchActivity.class);
                startActivity(intent);
            }
        });

        btnAddNewTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clMainTeam.setVisibility(View.GONE);
                clUpdateTeam.setVisibility(View.GONE);
                clAddTeam.setVisibility(View.VISIBLE);
            }
        });

        btnCreateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createTeam();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelTeam();
            }
        });

        //Edit Team
        clUpdateTeam = (ScrollView) findViewById(R.id.clUpdateTeam);

        edtUpdateName = (EditText) findViewById(R.id.edtUpdateName);
        spnUpdateCity = (Spinner) findViewById(R.id.spnUpdateCity);
        listUpdatePlayers = getPlayersOfTeam();
        btnAddUpdatePlayer = (Button) findViewById(R.id.btnUpdateAddPlayer);
        lvUpdatePlayers = (ListView) findViewById(R.id.lvUpdatePlayers);
        adapterUpdatePlayers = new AdapterPlayers(this, listUpdatePlayers);

        btnUpdateTeam = (Button) findViewById(R.id.btnUpdateTeam);
        btnUpdateCancel = (Button) findViewById(R.id.btnUpdateCancel);

        btnUpdateTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateTeam();
            }
        });

        btnUpdateCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelTeam();
            }
        });

        updateCitys();
    }

    private static void getTeams(final Activity activity) {
        Call <List <Team>> requestTeams = new RetrofitConfig().getEndpointsService().getTeamsByPlayer(MainActivity.loggedPlayer.getId());
        requestTeams.enqueue(new Callback <List <Team>>() {
            @Override
            public void onResponse(Call <List <Team>> call, Response <List <Team>> response) {
                if (response.isSuccessful()) {
                    List <Team> teams = response.body();

                    listTeams = teams;
                    adapterTeams = new AdapterTeams(activity, listTeams);
                    lvTeams.setAdapter(adapterTeams);
                } else {
                    Toast.makeText(activity, activity.getString(R.string.team_error_get), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call <List <Team>> call, Throwable t) {
                Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public static void enableUpdateTeam(Activity activity, int position) {
        updatingTeam = listTeams.get(position);

        edtUpdateName.setText(updatingTeam.getName());
        //spnUpdateCity
        listUpdatePlayers.clear();
        listUpdatePlayers.addAll(updatingTeam.getPlayers());
        adapterUpdatePlayers = new AdapterPlayers(activity, listUpdatePlayers);
        lvUpdatePlayers.setAdapter(adapterUpdatePlayers);

        clUpdateTeam.setVisibility(View.VISIBLE);
        clMainTeam.setVisibility(View.GONE);
        clAddTeam.setVisibility(View.GONE);
    }

    public static void removeTeam(final Activity activity, final Long idTeam) {
         AlertDialog.Builder dlg;

        dlg = new AlertDialog.Builder(activity);
        dlg.setMessage(activity.getString(R.string.team_delete));
        dlg.setNeutralButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Call <ResponseBody> deleteTeamRequest = new RetrofitConfig().getEndpointsService().deleteTeam(idTeam);
                deleteTeamRequest.enqueue(new Callback <ResponseBody>() {
                    @Override
                    public void onResponse(Call <ResponseBody> call, Response <ResponseBody> response) {
                        if (response.isSuccessful()) {


                            TeamsActivity.getTeams(activity);

                            Toast.makeText(activity, activity.getString(R.string.team_deleted), Toast.LENGTH_SHORT).show();
                        } else if (!response.isSuccessful()) {
                            Toast.makeText(activity, activity.getString(R.string.team_error_delete), Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call <ResponseBody> call, Throwable t) {
                        Toast.makeText(activity, activity.getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        dlg.setPositiveButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        dlg.show();
    }


    private List <Player> getPlayersOfTeam() {
        List <Player> players = new ArrayList <>();

        players.add(MainActivity.loggedPlayer);

        return players;
    }


    public static void addPlayerOfTeam(Player player, Activity activity) {
        if (clAddTeam.getVisibility() == View.VISIBLE) {
            listPlayers.add(player);
            adapterPlayers = new AdapterPlayers(activity, listPlayers);
            lvPlayers.setAdapter(adapterPlayers);
        } else if (clUpdateTeam.getVisibility() == View.VISIBLE) {
            listUpdatePlayers.add(player);
            adapterUpdatePlayers = new AdapterPlayers(activity, listUpdatePlayers);
            lvUpdatePlayers.setAdapter(adapterUpdatePlayers);
        }

        Toast.makeText(activity, activity.getString(R.string.team_playeradded), Toast.LENGTH_SHORT).show();
    }

    public static void removePlayerOfTeam(Activity activity, int position) {
        if (clAddTeam.getVisibility() == View.VISIBLE) {
            listPlayers.remove(position);
            adapterPlayers = new AdapterPlayers(activity, listPlayers);
            lvPlayers.setAdapter(adapterPlayers);
        } else if (clUpdateTeam.getVisibility() == View.VISIBLE) {
            listUpdatePlayers.remove(position);
            adapterUpdatePlayers = new AdapterPlayers(activity, listUpdatePlayers);
            lvUpdatePlayers.setAdapter(adapterUpdatePlayers);
        }
    }

    private void updateCitys() {
        List <String> list = new ArrayList <>();
        list.add("Curitiba");
        ArrayAdapter <String> dataAdapter = new ArrayAdapter <String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCity.setAdapter(dataAdapter);
        spnUpdateCity.setAdapter(dataAdapter);
    }

    private void createTeam() {
        if (validateCreate()) {
            Set setPlayers = new HashSet(listPlayers);
            Country country= new Country(new Long(1), "Brasil");
            State state = new State(new Long(1), "Paraná", country);
            City city = new City(new Long(1), "Curitiba", state);

            Team team = new Team(null, edtName.getText().toString(), city, setPlayers, MainActivity.loggedPlayer, MainActivity.loggedPlayer);
            Call <Team> playersRequest = new RetrofitConfig().getEndpointsService().createTeam(team);
            playersRequest.enqueue(new Callback <Team>() {
                @Override
                public void onResponse(Call <Team> call, Response <Team> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(TeamsActivity.this, getString(R.string.team_sucess), Toast.LENGTH_SHORT).show();
                        getTeams(TeamsActivity.this);
                        clMainTeam.setVisibility(View.VISIBLE);
                        clUpdateTeam.setVisibility(View.GONE);
                        clAddTeam.setVisibility(View.GONE);
                    } else if (!response.isSuccessful()) {
                        Toast.makeText(TeamsActivity.this, getString(R.string.team_error), Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(TeamsActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call <Team> call, Throwable t) {
                    Toast.makeText(TeamsActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    private void updateTeam() {
        if (validateEdit()) {
            Set setPlayers = new HashSet(listUpdatePlayers);

            updatingTeam.setName(edtUpdateName.getText().toString());
            updatingTeam.setPlayers(setPlayers);
            Call <Team> playersRequest = new RetrofitConfig().getEndpointsService().updateTeam(updatingTeam.getId(), updatingTeam);
            playersRequest.enqueue(new Callback <Team>() {
                @Override
                public void onResponse(Call <Team> call, Response <Team> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(TeamsActivity.this, getString(R.string.team_sucess), Toast.LENGTH_SHORT).show();
                        //Team team = response.body();
                        getTeams(TeamsActivity.this);
                        clMainTeam.setVisibility(View.VISIBLE);
                        clUpdateTeam.setVisibility(View.GONE);
                        clAddTeam.setVisibility(View.GONE);
                    } else if (!response.isSuccessful()) {
                        Toast.makeText(TeamsActivity.this, getString(R.string.team_error), Toast.LENGTH_SHORT).show();
                    } else
                        Toast.makeText(TeamsActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call <Team> call, Throwable t) {
                    Toast.makeText(TeamsActivity.this, getString(R.string.error_request), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void cancelTeam() {
        clAddTeam.setVisibility(View.GONE);
        clUpdateTeam.setVisibility(View.GONE);
        clMainTeam.setVisibility(View.VISIBLE);
    }

    private boolean validateCreate() {
        if (edtName.length() == 0) {
            edtName.setError(getString(R.string.error_name));

            if (edtName.isFocusable())
                edtName.requestFocus();

            return false;
        } else
            return true;
    }

    private boolean validateEdit() {
        if (edtUpdateName.length() == 0) {
            edtUpdateName.setError(getString(R.string.error_name));

            if (edtUpdateName.isFocusable())
                edtUpdateName.requestFocus();

            return false;
        } else
            return true;
    }
}
