package com.easyfoot.easyfoot.model;

public class TeamRanking {

    private Long id;
    private Team team;
    private double points;

    public TeamRanking(){}

    public TeamRanking(Long id, Team team, double points) {
        this.id = id;
        this.team = team;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public double getPoints() {
        return points;
    }

    public void setPoints(double points) {
        this.points = points;
    }
}
