package com.easyfoot.easyfoot;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.easyfoot.easyfoot.model.Conversation;
import java.util.List;

public class AdapterConversation extends BaseAdapter {
    private final Activity activity;
    private final List<Conversation> conversations;

    public AdapterConversation(Activity activity, List<Conversation> conversations) {
        this.activity = activity;
        this.conversations = conversations;
    }

    @Override
    public int getCount() {
        return conversations.size();
    }

    @Override
    public Object getItem(int position) {
        return conversations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = activity.getLayoutInflater().inflate(R.layout.adapter_conversation, parent, false);

        final Conversation conversation = conversations.get(position);
        LinearLayout llSender = (LinearLayout) view.findViewById(R.id.llSender);
        LinearLayout llReceiver = (LinearLayout) view.findViewById(R.id.llReceiver);

        if(conversation != null) {
            if (conversation.getPlayerSender().getId().equals(MainActivity.loggedPlayer.getId())) {

                TextView tvSender = (TextView) view.findViewById(R.id.tvSender);

                tvSender.setText(conversation.getMessage().toString());

                llReceiver.setVisibility(View.GONE);
            } else {

                TextView tvReceiver = (TextView) view.findViewById(R.id.tvReceiver);

                tvReceiver.setText(conversation.getMessage().toString());

                llSender.setVisibility(View.GONE);
            }
        }
        return view;
    }
}

